package ch.scs.feedbackloop.prototypes.mockbackend.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WriteAccessTest {

    @Test
    public void should_grant_write_access(){
        WriteAccess writeAccess = new WriteAccess();
        writeAccess.requestAccessFor("u1");

        assertEquals(writeAccess.getWritingUser(), "u1");
        assertNull(writeAccess.getRequestingUser());
        assertNull(writeAccess.getTimeNextUser());
    }

    @Test
    public void should_be_able_to_request(){
        WriteAccess writeAccess = new WriteAccess();
        writeAccess.requestAccessFor("u1");
        writeAccess.requestAccessFor("u2");

        assertEquals("u1", writeAccess.getWritingUser());
        assertEquals("u2", writeAccess.getRequestingUser());
        assertNotNull(writeAccess.getRequestingUser());
    }

    @Test
    public void should_decline_second_requester(){
        WriteAccess writeAccess = new WriteAccess();
        writeAccess.requestAccessFor("u1");
        writeAccess.requestAccessFor("u2");
        writeAccess.requestAccessFor("u3");

        assertEquals("u1", writeAccess.getWritingUser());
        assertEquals("u2", writeAccess.getRequestingUser());
        assertNotNull(writeAccess.getRequestingUser());
    }

    @Test
    public void should_grant_u2(){
        WriteAccess writeAccess = new WriteAccess();
        writeAccess.requestAccessFor("u1");
        writeAccess.requestAccessFor("u2");
        writeAccess.grantAccess();

        assertEquals("u2", writeAccess.getWritingUser());
        assertNull(writeAccess.getRequestingUser());
        assertNull(writeAccess.getRequestingUser());
    }


    @Test
    public void should_grant_u2_automatically_after_countdown() throws InterruptedException {
        WriteAccess writeAccess = new WriteAccess();
        writeAccess.requestAccessFor("u1");
        writeAccess.requestAccessFor("u2");
        Thread.sleep(WriteAccess.SECONDS_COUNT_DOWN * 1000L + 200);
        writeAccess.update();

        assertEquals("u2", writeAccess.getWritingUser());
        assertNull(writeAccess.getRequestingUser());
        assertNull(writeAccess.getRequestingUser());
    }

    @Test
    public void should_cancel_request() throws InterruptedException {
        WriteAccess writeAccess = new WriteAccess();
        writeAccess.requestAccessFor("u1");
        writeAccess.requestAccessFor("u2");
        Thread.sleep(200);
        writeAccess.cancelRequest();

        assertEquals("u1", writeAccess.getWritingUser());
        assertNull(writeAccess.getRequestingUser());
        assertNull(writeAccess.getRequestingUser());
    }

}
