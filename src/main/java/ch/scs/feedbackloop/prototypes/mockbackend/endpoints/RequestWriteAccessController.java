package ch.scs.feedbackloop.prototypes.mockbackend.endpoints;

import ch.scs.feedbackloop.prototypes.mockbackend.models.WriteAccess;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"http://localhost:4200"})
public class RequestWriteAccessController {

    // in the actual implementation we need such a WriteAccess object for each task, and end points for each task
    private WriteAccess writeAccess = new WriteAccess();

    /**
     * Registers the requestingUser if no other user is currently requesting write access, otherwise the request is declined.
     * @param requestingUser id (U-number) of the user requesting write access.
     * @return  a WriteAccess object representing the write access state of task666
     */
    @GetMapping("/task666/write-access/request")
    public WriteAccess requestWriteAccess(@RequestParam("for") String requestingUser) {
        System.out.println("requesting for: "+requestingUser);
        return writeAccess.requestAccessFor(requestingUser);
    }

    /**
     * Cancels current write access request
     * @return  a WriteAccess object representing the write access state of task666
     */
    @GetMapping("/task666/write-access/cancel")
    public WriteAccess cancelWriteAccessRequest() {
        return writeAccess.cancelRequest();
    }

    /**
     * Get the current write access state of this task
     * @return  a WriteAccess object representing the write access state of task666
     */
    @GetMapping("/task666/write-access/status")
    public WriteAccess getWriteAccessRequests() {
        return writeAccess.update();
    }

    /**
     * Current user with write access yields access to the current requester
     * @return  a WriteAccess object representing the write access state of task666
     */
    @GetMapping("/task666/write-access/grant")
    public WriteAccess grantWriteAccess() {
        return writeAccess.grantAccess();
    }

    /**
     * Current user with write access rejects request from another user
     * @return  a WriteAccess object representing the write access state of task666
     */
    @GetMapping("/task666/write-access/reject")
    public WriteAccess rejectRequest() {
        return writeAccess.rejectRequest();
    }
}
