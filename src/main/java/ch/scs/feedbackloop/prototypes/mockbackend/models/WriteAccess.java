package ch.scs.feedbackloop.prototypes.mockbackend.models;


import java.time.LocalDateTime;

/**
 * Object describing the state of write access of a particular "Expert Labeling Task"
 */
public class WriteAccess {
    /**
     * How many seconds till write access will be transferred to requesting user automatically. Default 10 seconds
     */
    public static int SECONDS_COUNT_DOWN = 10; //TODO configurable in properties file
    /**
     * User who currently possesses write access, null if none.
     */
    private String writingUser = null;

    /**
     * User who requested write access, null if none and after a new writingUser has been assigned
     */
    private String requestingUser = null;

    /**
     * Time when write access will be transferred to requestingUser
     */
    private LocalDateTime timeNextUser = null;

    public WriteAccess() {
    }

    /**
     * Updates the state considering the current time, specifically when the user with write access does not respond to a write access request by
     * another user
     * @return the write access state of an Expert Labeling Task
     */
    public WriteAccess update() {
        updateState();
        return this;
    }

    /**
     * Transfers write access to requesting user
     * @return the write access state of an Expert Labeling Task
     */
    public WriteAccess grantAccess() {
        if (requestingUser != null) {
            writingUser = requestingUser;
            requestingUser = null;
            timeNextUser = null;
        }
        return this;
    }

    /**
     * Rejects the request for write access of another user
     * @return the write access state of an Expert Labeling Task
     */
    public WriteAccess rejectRequest() {
        requestingUser = null;
        timeNextUser = null;
        return this;
    }

    /**
     * Register a write access request for user, only if no other request is in progress, yet.
     * @return the write access state of an Expert Labeling Task
     */
    public WriteAccess requestAccessFor(String user) {
        updateState();
        if (requestingUser == null) { // make sure no other user is already requesting write access
            requestingUser = user;
            timeNextUser = LocalDateTime.now().plusSeconds(SECONDS_COUNT_DOWN);
            if (writingUser == null) {
                return grantAccess();
            }
        }
        return this;
    }
    /**
     * Cancel a request before write access has been granted
     * @return the write access state of an Expert Labeling Task
     */
    public WriteAccess cancelRequest() {
        requestingUser = null;
        timeNextUser = null;
        return this;
    }

    // helpers
    private void updateState() {
        // grant access if time is up
        if (timeNextUser != null && LocalDateTime.now().isAfter(timeNextUser)) {
            grantAccess();
        }
    }


    // GETTERS
    public String getWritingUser() {
        return writingUser;
    }

    public String getRequestingUser() {
        return requestingUser;
    }

    public LocalDateTime getTimeNextUser() {
        return timeNextUser;
    }
}
